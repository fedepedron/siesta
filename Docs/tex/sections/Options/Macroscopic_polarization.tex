\begin{fdfentry}{PolarizationGrids}[block]
    \index{bulk polarization}%
    \index{Berry phase}

    If specified, the macroscopic polarization will be calculated using
    the geometric Berry phase approach (R.D. King-Smith, and
    D. Vanderbilt, PRB \textbf{47}, 1651 (1993)). In this method the
    electronic contribution to the macroscopic polarization, along a
    given direction, is calculated using a discretized version of the
    formula
    \begin{equation}
      \label{pol_formula}
      P_{e,\parallel}=\frac{ifq_e}{8\pi^3} \int_A d\mathbf{k}_\perp
      \sum_{n=1}^M \int_0^{|G_\parallel|} dk_{\parallel}
      \langle u_{\mathbf{k} n} |\frac\delta{\delta k_{\parallel}} |
      u_{\mathbf{k} n} \rangle
    \end{equation}
    where $f$ is the occupation (2 for a non-magnetic system), $q_e$ the
    electron charge, $M$ is the number of occupied bands (the system
    \textbf{must} be an insulator), and $u_{\mathbf{k} n}$ are the
    periodic Bloch functions. $\mathbf{G}_\parallel$ is the shortest
    reciprocal vector along the chosen direction.

    As it can be seen in formula \eqref{pol_formula}, to compute each
    component of the polarization we must perform a surface integration
    of the result of a 1-D integral in the selected direction.  The
    grids for the calculation along the direction of each of the three
    lattice vectors are specified in the block \fdf{PolarizationGrids}.
    \begin{fdfexample}
       %block PolarizationGrids
          10   3  4      yes
           2  20  2       no
           4   4 15
       %endblock PolarizationGrids
    \end{fdfexample}

    All three grids must be specified, therefore a $3\times3$ matrix of
    integer numbers must be given: the first row specifies the grid that
    will be used to calculate the polarization along the direction of
    the first lattice vector, the second row will be used for the
    calculation along the the direction of the second lattice vector,
    and the third row for the third lattice vector.  The numbers in the
    diagonal of the matrix specifie the number of points to be used in
    the one dimensional line integrals along the different
    directions. The other numbers specifie the mesh used in the surface
    integrals.  The last column specifies if the bidimensional grids are
    going to be diplaced from the origin or not, as in the
    Monkhorst-Pack algorithm (PRB \textbf{13}, 5188 (1976)).  This last
    column is optional.  If the number of points in one of the grids is
    zero, the calculation will not be performed for this particular
    direction.

    For example, in the given example, for the computation in the
    direction of the first lattice vector, 15 points will be used for
    the line integrals, while a $3\times4$ mesh will be used for the
    surface integration. This last grid will be displaced from the
    origin, so $\Gamma$ will not be included in the bidimensional
    integral. For the directions of the second and third lattice
    vectors, the number of points will be $20$ and $2\times2$, and $15$
    and $4\times4$, respectively.

    It has to be stressed that the macroscopic polarization can only be
    meaningfully calculated using this approach for insulators.
    Therefore, the presence of an energy gap is necessary, and no band
    can cross the Fermi level. The program performs a simple check of
    this condition, just by counting the electrons in the unit cell (
    the number must be even for a non-magnetic system, and the total
    spin polarization must have an integer value for spin polarized
    systems), however is the responsability of the user to check that
    the system under study is actually an insulator (for both spin
    components if spin polarized).

    The total macroscopic polarization, given in the output of the
    program, is the sum of the electronic contribution (calculated as
    the Berry phase of the valence bands), and the ionic contribution,
    which is simply defined as the sum of the atomic positions within
    the unit cell multiply by the ionic charges
    ($\sum_i^{N_a} Z_i \mathbf{r}_i$).  In the case of the magnetic
    systems, the bulk polarization for each spin component has been
    defined as
    \begin{equation}
      \mathbf{P}^\sigma = \mathbf{P}_e^\sigma +
      \frac12 \sum_i^{N_a}  Z_i \mathbf{r}_i
    \end{equation}
    $N_a$ is the number of atoms in the unit cell, and $\mathbf{r}_i$
    and $Z_i$ are the positions and charges of the ions.

    It is also worth noting, that the macroscopic polarization given by
    formula \eqref{pol_formula} is only defined modulo a ``quantum'' of
    polarization (the bulk polarization per unit cell is only well
    defined modulo $fq_e\mathbf{R}$, being $\mathbf{R}$ an arbitrary
    lattice vector). However, the experimentally observable quantities
    are associated to changes in the polarization induced by changes on
    the atomic positions (dynamical charges), strains (piezoelectric
    tensor), etc... The calculation of those changes, between different
    configurations of the solid, will be well defined as long as they
    are smaller than the ``quantum'', i.e. the perturbations are small
    enough to create small changes in the polarization.

  \end{fdfentry}

  \begin{fdflogicalF}{BornCharge}
    \index{Born effective charges}

    If true, the Born effective charge tensor is calculated for each
    atom by finite differences, by calculating the change in electric
    polarization (see \fdf{PolarizationGrids}) induced by the small
    displacements generated for the force constants calculation (see
    \fdf{MD.TypeOfRun:FC}):
    \begin{equation}
      \label{eq:effective_charge}
      Z^*_{i,\alpha,\beta}=\frac{\Omega_0}{e} \left. {\frac{\partial{P_\alpha}}
            {\partial{u_{i,\beta}}}}\right|_{q=0}
    \end{equation}
    where e is the charge of an electron and $\Omega_0$ is the unit cell
    volume.

    To calculate the Born charges it is necessary to specify both the
    Born charge flag and the mesh used to calculate the polarization,
    for example:
    \begin{fdfexample}
      %block PolarizationGrids
        7  3  3
        3  7  3
        3  3  7
      %endblock PolarizationGrids
      BornCharge True
    \end{fdfexample}

    The Born effective charge matrix is then written to the file
    \sysfile{BC}.

    The method by which the polarization is calculated may introduce an
    arbitrary phase (polarization quantum), which in general is far
    larger than the change in polarization which results from the atomic
    displacement. It is removed during the calculation of the Born
    effective charge tensor.

    The Born effective charges allow the calculation of LO-TO splittings
    and infrared activities. The version of the Vibra utility code in
    which these magnitudes are calculated is not yet distributed with
    \siesta, but can be obtained form Tom Archer (archert@tcd.ie).

  \end{fdflogicalF}



