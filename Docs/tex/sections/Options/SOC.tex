\siesta\ includes the option of including in the total Hamiltonian not only the Darwin
and velocity correction terms~(Scalar--Relativistic calculations), but
also the spin-orbit~(SO) contribution.  See \fdf{Spin} on how to
enable spin-orbit coupling.

The SO functionality has been implemented by
Dr. Ram\'on Cuadrado and Dr. Jorge I. Cerd\'a based on their initial
work~(R. Cuadrado and J. I. Cerd\'a ``Fully relativistic pseudopotential
formalism under an atomic orbital basis: spin-orbit splittings and
magnetic anisotropies'', J. Phys.: Condens. Matter \textbf{24}, 086005 (2012);
``In-plane/out-of-plane disorder influence on the magnetic anisotropy of
Fe$_{1-y}$Mn$_y$Pt-L1(0) bulk alloy'', R. Cuadrado, Kai Liu, Timothy
J. Klemmer and R. W. Chantrell, Applied Physics Letters, \textbf{108},
123102 (2016)).

The inclusion of the SO term in the Hamiltonian (and in the Density
Matrix) causes an increase in the number of non-zero elements in their
off-diagonal parts, i.e., for some $(\mu,\nu)$ pair of basis
orbitals, $\mathbf H^{\sigma\sigma'}_{\mu\nu}$ ($\mathbf{DM}^{\sigma\sigma'}_{\mu\nu}$)
[$\sigma,\sigma'=\uparrow,\downarrow$] will be $\neq0$. This is
mainly due to the fact that the $\mathbf L\cdot\mathbf S$ operator
will promote the mixing between different spin-up/down components.
In addition, these $\mathbf H^{\sigma\sigma'}_{\mu\nu}$ (and
$\mathbf{DM}^{\sigma\sigma'}_{\mu\nu}$) elements will be complex, in contrast
with typical polarized/non-polarized calculations where these
matrices are purely real. Since the spin-up and spin-down manifolds
are essentially mixed, the solver has to deal with matrices whose
dimensions are twice as large as for the collinear (unmixed) spin
problem. Due to this, we advise to take special
attention to the memory needed to perform a spin-orbit calculation.

Apart from the study of effects of the spin--orbit interaction in the
band structure, a feature enabled by a SO formalism is the computation
of the Magnetic Anisotropy Energy~(MAE): it can be obtained as the
difference in the total selfconsistent energy in two different spin
orientations, usually along the easy axis and the hard axis. In
\siesta\ it is possible to perform calculations for different
magnetization orientations using the block \fdf{DM.InitSpin} in the
fdf file. In doing so one will be able to include the initial
orientation angles of the magnetization for each atom, as well as an
initial value of their net magnetic moments. See
also the recent review~\cite{doi:10.1063/5.0005077}.

Note: Due to the small contribution of the spin--orbit interaction to
the total energy, the level of precision required is quite high. The
following parameters should be carefully checked for each
specific system to assure that the results are converged and accurate enough:
\fdf{SCF.H!Tolerance} during the selfconsistency~(typically
<10$^{-4}$eV), \fdf{ElectronicTemperature}, \textbf{k}-point sampling,
and \fdf{Mesh!Cutoff}~(specifically for extended
solids). In general, one can say that a good calculation will have
a high number of k--points, low \fdf{ElectronicTemperature}, very
small \fdf{SCF.H!Tolerance} and high values of \fdf{Mesh!Cutoff}.  We
encourage the user to test carefully these options for each system.

An additional point to take into account when the spin--orbit
contribution is included is the mixing scheme to use. You are
encouraged to use the option to mix the Hamiltonian (\fdf{SCF.Mix} \fdf*{hamiltonian}) instead of the
density matrix to speed up convergence. In addition, the pseudopotentials
have to be well tested for each specific system. They
have to be generated in their fully relativistic form, and should use
non-linear core corrections. Finally it is worth to mention that the
selfconsistent convergence for some non-highly symmetric
magnetizations directions with respect to the physical symmetry axis
could still be difficult.

\begin{fdfentry}{Spin!OrbitStrength}[real]<1.0>

  It allows to vary the strength of the
  spin-orbit interaction from zero to any positive value. It should be
  used only for debugging and testing purposes, as the only physical value is 1.0.
  Note that this feature is currently implemented by modifying the SO parts of the
  semilocal potentials read from a \code{.psf} file. It will not work
  when reading the $lj$ projectors directly from a PSML file (or from
  a previous run's \code{.ion} file). Care must be
  taken when re-using any \code{.ion} files produced.

\end{fdfentry}

\begin{fdflogicalF}{WriteOrbMom}

  If \fdftrue, a table is provided in the output file that
  includes an estimation of the vector orbital magnetic
  moments, in units of the Bohr magneton, projected
  onto each orbital and also onto each atom. The estimation for the
  orbital moments is based on a two-center approximation, and makes use
  of the Mulliken population analysis.

  If \fdf{MullikenInScf} is \fdftrue, this information is printed at
  every scf step.

\end{fdflogicalF}

\begin{fdflogicalT}{SOC.Split.SR.SO}

  In calculations with spin-orbit-coupling (SOC) the program carries
  out a splitting of the contributions to the Hamiltonian and energies
  into scalar-relativistic (SR) and spin-orbit (SO) parts. The
  splitting procedure when lj projectors are involved can sometimes be
  ill-defined, and in those cases the program relies on a heuristic to
  compute the two contributions. A warning is printed.

  If this option is set to \fdffalse, it will prevent the program from
  attempting the splitting (but it still will be able to detect a
  possible problem and report an informational message).

  When the SO contribution is not split, the relevant energy
  contributions in the output file are tagged
  \code{Enl(+so)} and \code{Eso(nil)}.

  The CML file is not thus changed (but there is a new parameter
  \code{Split-SR-SO}).

  Note that this is only a cosmetic change affecting the reporting of
  some components of the energy. All the other results should be
  unchanged.

\end{fdflogicalT}

\subsubsection{On-site approximation}
\label{sec:onsite-SOC}
Within the so-called ``on-site'' approximation only the intra-atomic SO
contribution is taken into account, neglecting three-center SO matrix elements.

The on-site spin-orbit scheme in this version of \siesta\ has been
implemented by Dr. Ram\'on Cuadrado based on the original formalism
and implementation developed by Prof. Jaime Ferrer and his
collaborators (L Fern\'andez--Seivane, M Oliveira, S Sanvito, and J
Ferrer, Journal of Physics: Condensed Matter, \textbf{18}, 7999
(2006); L Fern\'andez--Seivane and Jaime Ferrer,
Phys. Rev. Lett. \textbf{99}, 183401 (2007)).  183401).

It should be noted that this approximation, while based on the
physically reasonable idea of the short-range of the SO interaction,
is susceptible to some
inaccuracies~\cite{doi:10.1103/PhysRevB.104.195104}.  Since the
construction of the full SOC Hamiltonian represents a small fraction
of the computational effort, the performance gains in using the
on-site approximation are negligible and do not justify its
use. Hence, the full SO formalism is used by default, being necessary
to change the \fdf{Spin} flag in the input file if the on-site
approximation is desired.
