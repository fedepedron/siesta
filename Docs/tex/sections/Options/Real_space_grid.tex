
\siesta\ uses a finite 3D grid for the calculation of some
integrals and the representation of charge densities and potentials.
Its fineness is determined by its plane-wave cutoff, as
given by the \fdf{Mesh!Cutoff}option. It means that all periodic
plane waves with kinetic energy lower than this cutoff
can be represented in the grid without aliasing. In turn,
this implies that if a function (e.g. the density or the
effective potential) is an expansion of
only these plane waves, it can be Fourier transformed
back and forth without any approximation.

The existence of the grid causes the breaking of translational
symmetry (the egg-box effect, due to the fact that the density
and potential \emph{do have} plane wave components above
the mesh cutoff).  This symmetry breaking is clear when
moving one single atom in an otherwise empty simulation cell. The
total energy and the forces oscillate with the grid periodicity when
the atom is moved, as if the atom were moving on an eggbox. In the
limit of infinitely fine grid (infinite mesh cutoff) this effect
disappears.

For reasonable values of the mesh cutoff, the effect of the eggbox
on the total energy or on the relaxed structure is normally unimportant.
However, it can affect substantially the process of relaxation, by
increasing the number of steps considerably, and can also spoil the
calculation of vibrations, usually much more demanding than relaxations.

The \program{Util/Scripting/eggbox\_checker.py} script can be used to
diagnose the eggbox effect to be expected for a particular
pseudopotential/basis-set combination.

Apart from increasing the mesh cutoff (see the \fdf{Mesh!Cutoff} option),
the following options might help in lessening a given eggbox problem. But
note also that a filtering of the orbitals and the relevant parts of
the pseudopotential and the pseudocore charge might be enough to solve
the issue (see Sec.~\ref{sec:filtering}).

\begin{fdfentry}{Mesh!Cutoff}[energy]<$300\,\mathrm{Ry}$>
  \index{grid}%
  \index{mesh}

  Defines the plane wave cutoff for the grid.

  % JMS/AG. To be implemented:
  % \textit{Default value:} If not present, \fdf{Mesh!Cutoff} is made equal
  % to \fdf{FilterCutoff}, if present. If not, it is obtained from
  % \fdf{FilterTol}, if present, as explained in that parameter.
  % If none of these parameters is present, the default value for
  % \fdf{Mesh!Cutoff} is \texttt{100 Ry}

\end{fdfentry}

\begin{fdfentry}{Mesh!Sizes}[list]<\fdfvalue{Mesh!Cutoff}>
  \index{grid}%
  \index{mesh}

  Manual definition of grid size along each lattice vector. The value
  must be divisible by \fdf{Mesh!SubDivisions}, otherwise the
  program will die. The numbers should also be divisible with $2$, $3$
  and $5$ due to the FFT algorithms.

  This option may be specified as a block, or a list:
  \begin{fdfexample}
    %block Mesh.Sizes
      100 202 210
    %endblock
    # Or equivalently:
    Mesh.Sizes [100 202 210]
  \end{fdfexample}

  By default the grid size is determined via \fdf{Mesh!Cutoff}. This
  option has precedence if both are specified.

\end{fdfentry}

\begin{fdfentry}{Mesh!SubDivisions}[integer]<$2$>
  \index{grid}%
  \index{mesh}

  Defines the number of sub-mesh points in each direction used
  to save index storage on the mesh. It affects the memory
  requirements and the CPU time, but not the results.

  \note The default value might be a bit conservative. Users might
  experiment with higher values, 4 or 6, to lower the memory and
  cputime usage.

\end{fdfentry}

\begin{fdfentry}{Grid.CellSampling}[block]
  \index{egg-box effect}%
  \index{rippling}

  It specifies points within the grid cell for a symmetrization
  sampling.

  For a given grid the grid-cutoff convergence can be improved (and
  the eggbox lessened) by recovering the lost symmetry: by
  symmetrizing the sensitive quantities. The full symmetrization
  implies an integration (averaging) over the grid cell. Instead, a
  finite sampling can be performed.

  It is a sampling of rigid displacements of the system with respect
  to the grid. The original grid-system setup (one point of the grid
  at the origin) is always calculated. It is the (0,0,0) displacement.
  The block \fdf{Grid.CellSampling} gives the additional
  displacements wanted for the sampling. They are given relative to
  the grid-cell vectors, i.e., (1,1,1) would displace to the next grid
  point across the body diagonal, giving an equivalent grid-system
  situation (a useless displacement for a sampling).

  Examples: Assume a cubic cell, and therefore a (smaller) cubic grid
  cell.  If there is no block or the block is empty, then the original
  (0,0,0) will be used only. The block:
  \begin{fdfexample}
     %block Grid.CellSampling
        0.5    0.5    0.5
     %endblock Grid.CellSampling
  \end{fdfexample}
  would use the body center as a second point in the sampling. Or:
  \begin{fdfexample}
     %block Grid.CellSampling
        0.5    0.5    0.0
        0.5    0.0    0.5
        0.0    0.5    0.5
     %endblock Grid.CellSampling
  \end{fdfexample}
  gives an fcc kind of sampling, and
  \begin{fdfexample}
     %block Grid.CellSampling
        0.5    0.0    0.0
        0.0    0.5    0.0
        0.0    0.0    0.5
        0.0    0.5    0.5
        0.5    0.0    0.5
        0.5    0.5    0.0
        0.5    0.5    0.5
     %endblock Grid.CellSampling
  \end{fdfexample}
  gives again a cubic sampling with half the original side length.  It
  is not trivial to choose a right set of displacements so as to
  maximize the new 'effective' cutoff. It depends on the kind of
  cell. It may be automatized in the future, but it is now left to the
  user, who introduces the displacements manually through this block.

  The quantities which are symmetrized are: ($i$) energy terms that
  depend on the grid, ($ii$) forces, ($iii$) stress tensor, and ($iv$)
  electric dipole.

  The symmetrization is performed at the end of every SCF cycle. The
  whole cycle is done for the (0,0,0) displacement, and, when the
  density matrix is converged, the same (now fixed) density matrix is
  used to obtain the desired quantities at the other displacements
  (the density matrix itself is \emph{not} symmetrized as it gives a
  much smaller egg-box effect). The CPU time needed for each
  displacement in the \fdf{Grid.CellSampling} block is of the order
  of one extra SCF iteration.

  This may be required in systems where very precise forces are needed,
  and/or if partial cores are used. It is advantageous to test whether
  the forces are sampled sufficiently by sampling one point.

  Additionally this may be given as a list of 3 integers which
  corresponds to a ``Monkhorst-Pack'' like grid sampling. I.e.
  \begin{fdfexample}
     Grid.CellSampling [2 2 2]
  \end{fdfexample}
  is equivalent to
  \begin{fdfexample}
     %block Grid.CellSampling
        0.5    0.0    0.0
        0.0    0.5    0.0
        0.5    0.5    0.0
        0.0    0.0    0.5
        0.5    0.0    0.5
        0.0    0.5    0.5
        0.5    0.5    0.5
     %endblock Grid.CellSampling
  \end{fdfexample}
  This is an easy method to see if the flag is important for your
  system or not.

\end{fdfentry}

\begin{fdfentry}{EggboxRemove}[block]
  \index{egg-box effect}%
  \index{rippling}

  For recovering translational invariance in an approximate way.

  It works by substracting from Kohn-Sham's total energy (and forces)
  an approximation to the eggbox energy, sum of atomic contributions.
  Each atom has a predefined eggbox energy depending on where it sits
  on the cell. This atomic contribution is species dependent and is
  obviously invariant under grid-cell translations. Each species
  contribution is thus expanded in the appropriate Fourier series.  It
  is important to have a smooth eggbox, for it to be represented by a
  few Fourier components. A jagged egg-box (unless very small, which
  is then unimportant) is often an indication of a problem with the
  pseudo.

  In the block there is one line per Fourier component. The first
  integer is for the atomic species it is associated with. The other
  three represent the reciprocal lattice vector of the grid cell (in
  units of the basis vectors of the reciprocal cell). The real number
  is the Fourier coefficient in units of the energy scale given in
  \fdf{EggboxScale} (see below), normally 1 eV.

  The number and choice of Fourier components is free, as well as
  their order in the block. One can choose to correct only some
  species and not others if, for instance, there is a substantial
  difference in hardness of the cores. The 0 0 0 components will add a
  species-dependent constant energy per atom. It is thus irrelevant
  except if comparing total energies of different calculations, in
  which case they have to be considered with care (for instance by
  putting them all to zero, i.e. by not introducing them in the list).
  The other components average to zero representing no bias in the
  total energy comparisons.

  If the total energies of the free atoms are put as 0 0 0
  coefficients (with spin polarisation if adequate etc.) the corrected
  total energy will be the cohesive energy of the system (per unit
  cell).

  \emph{Example:} For a two species system, this example would give a
  quite sufficent set in many instances (the actual values of the
  Fourier coefficients are not realistic).
  \begin{fdfexample}
     %block EggBoxRemove
       1   0   0   0 -143.86904
       1   0   0   1    0.00031
       1   0   1   0    0.00016
       1   0   1   1   -0.00015
       1   1   0   0    0.00035
       1   1   0   1   -0.00017
       2   0   0   0 -270.81903
       2   0   0   1    0.00015
       2   0   1   0    0.00024
       2   1   0   0    0.00035
       2   1   0   1   -0.00077
       2   1   1   0   -0.00075
       2   1   1   1   -0.00002
     %endblock EggBoxRemove
  \end{fdfexample}

  It represents an alternative to grid-cell sampling (above).  It is
  only approximate, but once the Fourier components for each species
  are given, it does not represent any computational effort (neither
  memory nor time), while the grid-cell sampling requires CPU time
  (roughly one extra SCF step per point every MD step).

  It will be particularly helpful in atoms with substantial partial
  core or semicore electrons.

  \note This should only be used for fixed cell calculations, i.e. not
  with \fdf{MD.VariableCell}.

  For the time being, it is up to the user to obtain the Fourier
  components to be introduced. They can be obtained by moving one
  isolated atom through the cell to be used in the calculation (for a
  give cell size, shape and mesh), once for each species.  The
  \program{Util/Scripting/eggbox\_checker.py} script can be used as a starting
  point for this.

\end{fdfentry}

\begin{fdfentry}{EggboxScale}[energy]<$1\,\mathrm{eV}$>
  \index{egg-box effect}%
  \index{rippling}

  Defines the scale in which the Fourier components of the egg-box
  energy are given in the \fdf{EggboxRemove} block.

\end{fdfentry}