
\label{sec:lua}

Since \siesta\ 4.1 an additional method of controlling the convergence
and MD of \siesta\ is enabled through external scripting
capability. The external control comes in two variants:
\begin{itemize}
  \item Implicit control of MD through updating/changing parameters
  and optimizing forces. For instance one may use a \fdf*{Verlet} MD
  method but additionally update the forces through some external
  force-field to amend limitations by the \fdf*{Verlet} method for
  your particular case. In the implicit control the molecular dynamics
  is controlled by \siesta.

  \item Explicit control of MD. In this mode the molecular dynamics
  \emph{must} be controlled in the external Lua script and the
  convergence of the geometry should also be controlled via this
  script.

\end{itemize}

The implicit control is in use if \fdf{MD.TypeOfRun} is something
other than \fdf*{lua}, while if the option is \fdf*{lua} the explicit
control is in use.

For examples on the usage of the Lua scripting engine and the power
you may find the library \program{flos}\footnote{This library is
    implemented by Nick R. Papior to further enhance the
    inter-operability with \siesta\ and external contributions.}, see
\url{https://github.com/siesta-project/flos}. At the time of writing
the \program{flos} library already implements new geometry/cell
relaxation schemes and new force-constants algorithms. You are highly
encouraged to use the new relaxation schemes as they may provide
faster convergence of the relaxation.

\begin{fdfentry}{Lua!Script}[file]<\nonvalue{none}>

  Specify a Lua script file which may be used to control the internal
  variables in \siesta. Such a script file must contain at least one
  function named \program{siesta\_comm} with no arguments.

  An example file could be this (note this is Lua code):
  \begin{codeexample}
-- This function (siesta_comm) is REQUIRED
function siesta_comm()

   -- Define which variables we want to retrieve from SIESTA
   get_tbl = {"geom.xa", "E.total"}

   -- Signal to SIESTA which variables we want to explore
   siesta.receive(get_tbl)

   -- Now we have the required variables,
   -- convert to a simpler variable name (not nested tables)
   -- (note the returned quantities are in SIESTA units (Bohr, Ry)
   xa = siesta.geom.xa
   Etot = siesta.E.total

   -- If we know our energy is wrong by 0.001 Ry we may now
   -- change the total energy
   Etot = Etot - 0.001

   -- Return to SIESTA the total energy such that
   -- it internally has the "correct" energy.

   siesta.E.total = Etot
   ret_tbl = {"E.total"}

   siesta.send(ret_tbl)

end
\end{codeexample}

  Within this function there are certain \emph{states} which defines
  different execution points in \siesta:
  \begin{fdfoptions}

    \option[Initialization]%
    This is right after \siesta\ has read the options from the FDF
    file. Here you may query some of the FDF options (and even change
    them) for your particular problem.

    \note \program{siesta.state == siesta.INITIALIZE}.

    \option[Initialize-MD]%
    Right before the SCF step starts. This point is somewhat
    superfluous, but is necessary to communicate the actual meshcutoff
    used\footnote{Remember that the \fdf{Mesh!Cutoff} defined is the
        minimum cutoff used.}.

    \note \program{siesta.state == siesta.INIT\_MD}.

    \option[SCF]%
    Right after \siesta\ has calculated the output density matrix, and
    just after \siesta\ has performed mixing.

    \note \program{siesta.state == siesta.SCF\_LOOP}.

    \option[Forces]%
    This stage is right after \siesta\ has calculated the forces.

    \note \program{siesta.state == siesta.FORCES}.

    \option[Move]%
    This state will \emph{only} be reached if \fdf{MD.TypeOfRun} is
    \fdf*{lua}.

    If one does not return updated atomic coordinates \siesta\ will
    reuse the same geometry as just analyzed.

    \note \program{siesta.state == siesta.MOVE}.

    \option[After-move]%
    Right after determining the atomic coordinates for the next step.
    Therefore, this is the first thing that is done with the new
    atomic coordinates.

    \note \program{siesta.state == siesta.AFTER\_MOVE}.


    \option[Analysis]%
    Just before \siesta\ completes and exits.

    \note \program{siesta.state == siesta.ANALYSIS}.

  \end{fdfoptions}

  Beginning with implementations of Lua scripts may be cumbersome. It
  is recommended to start by using \program{flos}, see
  \url{https://github.com/siesta-project/flos} which contains several
  examples on how to start implementing your own scripts.
  Currently \program{flos} implements a larger variety of relaxation
  schemes, for instance:
  \begin{codeexample}
    local flos = require "flos"
    LBFGS = flos.LBFGS()
    function siesta_comm()
       LBFGS:SIESTA(siesta)
    end
  \end{codeexample}
  which is the most minimal example of using the L-BFGS algorithm for
  geometry relaxation. Note that \program{flos} reads the parameters
  \fdf{MD.MaxDispl} and \fdf{MD.MaxForceTol} through \siesta\
  automatically.

  \note The number of available variables continues to grow and to
  find which quantities are accessible in Lua you may add this small
  code in your Lua script:
  \begin{codeexample}
    siesta.print_allowed()
  \end{codeexample}
  which prints out a list of all accessible variables (note they are
  not sorted).

  If there are any variables you require which are not in the list,
  please contact the developers.

  If you want to stop \siesta\ from Lua you can use the following:
  \begin{codeexample}
    siesta.Stop = true
    siesta.send({"Stop"})
  \end{codeexample}
  which will abort \siesta.

  Remark that since \emph{anything} may be changed via Lua one may
  easily make \siesta\ crash due to inconsistencies in the internal
  logic. This is because \siesta\ does not check what has changed, it
  accepts everything \emph{as is} and continues. Hence, one should be
  careful what is changed.

\end{fdfentry}

\begin{fdflogicalF}{Lua!Debug}

  Debug the Lua script mode by printing out (on stdout) information
  everytime \siesta\ communicates with Lua.

\end{fdflogicalF}

\begin{fdflogicalF}{Lua!Debug.MPI}

  Debug all nodes (if in a parallel run).

\end{fdflogicalF}

\begin{fdflogicalF}{Lua!Interactive}

  Start an interactive Lua session at all the states in the program
  and ask for user-input.
  %
  This is primarily intended for debugging purposes. The interactive
  session is executed just \emph{before} the \code{siesta\_comm}
  function call (if the script is used).

  For serial runs \code{siesta.send} may be used. For parallel runs do
  \emph{not} use \code{siesta.send} as the code is only
  executed on the first MPI node.

  There are various commands that are caught if they are the only
  content on a line:
  \begin{fdfoptions}
    \option[/debug]%
    Turn on/off debugging information.

    \option[/show]%
    Show the currently collected lines of code.

    \option[/clear]%
    Clears the currently collected lines of code.

    \option[;]%
    Run the currently collected lines of code and continue collecting
    lines.

    \option[/run]%
    Same as \code{;}.

    \option[/cont]%
    Run the currently collected lines of code and continue \siesta.

    \option[/stop]%
    Run the currently collected lines of code and stop all future
    interactive Lua sessions.

  \end{fdfoptions}

  Currently this only works if \fdf{Lua!Script} is having a valid Lua
  file (note the file may be empty).

\end{fdflogicalF}



\subsection{Examples of Lua programs}

Please look in the \program{Tests/lua\_*} folders where examples of
basic Lua scripts are found. Below is a description of the \program{*}
examples.


\begin{description}
  \item[h2o] Changes the mixing weight continuously in
  the SCF loop. This will effectively speed up convergence time if one
  can attain the best mixing weight per SCF-step.

  \item[si111] Change the mixing method based on certain convergence
  criteria. I.e. after a certain convergence one can switch to a more
  aggressive mixing method.

\end{description}

A combination of the above two examples may greatly improve
convergence, however, creating a generic method to adaptively change
the mixing parameters may be very difficult to implement. If you do
create such a Lua script, please share it on the mailing list.


\subsection{External MD/relaxation methods}

Using the Lua interface allows a very easy interface for creating
external MD and/or relaxation methods.

A public library (\program{flos},
\url{https://github.com/siesta-project/flos}) already implements a
wider range of relaxation methods than intrinsically enabled in
\siesta. Secondly, by using external scripting mechanisms one can
customize the routines to a much greater extend while simultaneously
create custom constraints.

You are \emph{highly} encouraged to try out the \program{flos} library
(please note that \program{flook} is required, see installation
instructions above).


