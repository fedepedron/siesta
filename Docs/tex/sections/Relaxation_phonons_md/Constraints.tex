\textbf{Note:} The Zmatrix format (see Sec.~\ref{sec:Zmatrix}) provides
an alternative constraint formulation which can be useful for system
involving molecules.

\begin{fdfentry}{Geometry!Constraints}[block]
  \index{constraints in relaxations}

  Constrains certain atomic coordinates or cell parameters in a
  consistent method.

  There are a high number of configurable parameters that may be used
  to control the relaxation of the coordinates.

  \note \siesta\ prints out a small section of how the constraints are
  recognized.

  \def\directionalconstraint{\note these specifications are apt for \emph{directional}
    constraints.}


  \begin{fdfoptions}
    \option[atom|position]%
    Fix certain atomic coordinates.

    This option takes a variable number of integers which each
    correspond to the atomic index (or input sequence) in
    \fdf{AtomicCoordinatesAndAtomicSpecies}.

    \fdf*{atom} is now the preferred input option while
    \fdf*{position} still works for backwards compatibility.

    One may also specify ranges of atoms according to:

    \begin{fdfoptions}
      \option[{atom \emph{A} [\emph{B} [\emph{C} [\dots]]]}]%
      A sequence of atomic indices which are constrained.

      % Generic input (compatible with the <= 4.0)
      \option[{atom from \emph{A} to \emph{B} [step \emph{s}]}]%
      Here atoms \emph{A} up to and including \emph{B} are
      constrained.
      %
      If \fdf*{step <s>} is given, the range
      \emph{A}:\emph{B} will be taken in steps of \emph{s}.

      \begin{fdfexample}
        atom from 3 to 10 step 2
      \end{fdfexample}
      will constrain atoms 3, 5, 7 and 9.

      \option[{atom from \emph{A} plus/minus \emph{B} [step
          \emph{s}]}]%
      Here atoms \emph{A} up to and including $\emph{A}+\emph{B}-1$
      are constrained.
      %
      If \fdf*{step <s>} is given, the range
      \emph{A}:$\emph{A}+\emph{B}-1$ will be taken in steps of
      \emph{s}.

      % Generic input (compatible with the <= 4.0)
      \option[atom {[\emph{A}, \emph{B} -\mbox{}- \emph{C} [step \emph{s}], \emph{D}]}]%
      Equivalent to \fdf*{from \dots to} specification, however in a
      shorter variant. Note that the list may contain arbitrary number
      of ranges and/or individual indices.

      \begin{fdfexample}
        atom [2, 3 -- 10 step 2, 6]
      \end{fdfexample}
      will constrain atoms 2, 3, 5, 7, 9 and 6.

      \begin{fdfexample}
        atom [2, 3 -- 6, 8]
      \end{fdfexample}
      will constrain atoms 2, 3, 4, 5, 6 and 8.

      \option[atom all]%
      Constrain all atoms.

    \end{fdfoptions}

    \directionalconstraint


    \option[Z]%
    Equivalent to \fdf*{atom} with all indices of the atoms that
    have atomic number equal to the specified number.

    \directionalconstraint


    \option[species-i]%
    Equivalent to \fdf*{atom} with all indices of the atoms that
    have species according to the \fdf{ChemicalSpeciesLabel} and
    \fdf{AtomicCoordinatesAndAtomicSpecies}.

    \directionalconstraint


    \option[center]%
    One may retain the coordinate center of a
    range of atoms (say molecules or other groups of atoms).

    Atomic indices may be specified according to \fdf*{atom}.

    \directionalconstraint


    \option[rigid|molecule]%
    Move a selection of atoms together as though they where one atom.

    The forces are summed and averaged to get a net-force on the
    entire molecule.

    Atomic indices may be specified according to \fdf*{atom}.

    \directionalconstraint


    \option[rigid-max|molecule-max]%
    Move a selection of atoms together as though they where one atom.

    The maximum force acting on one of the atoms in the selection will
    be expanded to act on all atoms specified.

    Atomic indices may be specified according to \fdf*{atom}.

    \directionalconstraint


    \option[cell-angle]%
    Control whether the cell angles ($\alpha$, $\beta$, $\gamma$) may
    be altered.

    This takes either one or more of
    \fdf*{alpha}/\fdf*{beta}/\fdf*{gamma} as argument.

    \fdf*{alpha} is the angle between the 2nd and 3rd cell vector.

    \fdf*{beta} is the angle between the 1st and 3rd cell vector.

    \fdf*{gamma} is the angle between the 1st and 2nd cell vector.

    \note currently only one angle can be constrained at a time and it
    forces only the spanning vectors to be relaxed.


    \option[cell-vector]%
    Control whether the cell vectors ($A$, $B$, $C$) may be altered.

    This takes either one or more of \fdf*{A}/\fdf*{B}/\fdf*{C} as
    argument.

    Constraining the cell-vectors are only allowed if they only have a
    component along their respective Cartesian
    direction. I.e. \fdf*{B} must only have a $y$-component.


    \option[stress]%
    Control which of the 6 stress components are constrained.

    Numbers $1\le i\le6$ where $1$ corresponds
    to the \emph{XX} stress-component, $2$ is \emph{YY}, $3$ is
    \emph{ZZ}, $4$ is \emph{YZ}/\emph{ZY}, $5$ is \emph{XZ}/\emph{ZX}
    and $6$ is \emph{XY}/\emph{YX}.

    The text specifications are also allowed.


    \option[routine]%
    This calls the \program{constr} routine specified in the file:
    \file{constr.f}. Without having changed the corresponding source
    file, this does nothing.
    See details and comments in the source-file.


    \option[clear]%
    Remove constraints on selected atoms from all previously specified
    constraints.

    This may be handy when specifying constraints via \fdf*{Z} or
    \fdf*{species-i}.

    Atomic indices may be specified according to \fdf*{atom}.


    \option[clear-prev]
    Remove constraints on selected atoms from the \emph{previous} specified
    constraint.

    This may be handy when specifying constraints via \fdf*{Z} or
    \fdf*{species-i}.

    Atomic indices may be specified according to \fdf*{atom}.

    \note two consecutive \fdf*{clear-prev} may be used in conjunction
    as though the atoms where specified on the same line.

  \end{fdfoptions}

  It is instructive to give an example of the input options presented.

  Consider a benzene molecule ($\mathrm{C}_6\mathrm{H}_6$) and we wish
  to relax all Hydrogen atoms (and no stress in $x$ and $y$
  directions). This may be accomplished with this
  \begin{fdfexample}
    %block Geometry.Constraints
      Z 6
      stress 1 2
    %endblock
  \end{fdfexample}
  Or as in this example
  \begin{fdfexample}
    %block AtomicCoordinatesAndAtomicSpecies
      ... ... ... 1   # C 1
      ... ... ... 2   # H 2
      ... ... ... 1   # C 3
      ... ... ... 2   # H 4
      ... ... ... 1   # C 5
      ... ... ... 2   # H 6
      ... ... ... 1   # C 7
      ... ... ... 2   # H 8
      ... ... ... 1   # C 9
      ... ... ... 2   # H 10
      ... ... ... 1   # C 11
      ... ... ... 2   # H 12
      stress XX YY
    %endblock
    %block Geometry.Constraints
      atom from 1 to 12 step 2
      stress XX YY
    %endblock
    %block Geometry.Constraints
      atom [1 -- 12 step 2]
      stress XX 2
    %endblock
    %block Geometry.Constraints
      atom all
      clear-prev [2 -- 12 step 2]
      stress 1 YY
    %endblock
  \end{fdfexample}
  where the 3 last blocks all create the same result.

  Finally, the \emph{directional} constraint is an important and often
  useful feature.
  The directional constraints will subtract the force projected onto
  the direction specified. Hence an $x$ directional constraint will
  remove the force component along the $x$ direction $f_x\to 0$.

  When relaxing complex structures it may be advantageous to first
  relax along a given direction (where you expect the stress to be the
  largest) and subsequently let it fully relax. Another example would
  be to relax the binding distance between a molecule and a surface,
  before relaxing the entire system by forcing the molecule and
  adsorption site to relax together.
  %
  To use directional constraints one may provide an additional 3
  \emph{reals} after the \fdf*{atom}/\fdf*{rigid}.
  For instance in the previous example (benzene) one may first relax
  all Hydrogen atoms along the $y$ and $z$ Cartesian vector by
  constraining the $x$ Cartesian vector
  \begin{fdfexample}
    %block Geometry.Constraints
      Z 6 # constrain Carbon
      Z 1 1. 0. 0. # constrain Hydrogen along x Cartesian vector
    %endblock
  \end{fdfexample}
  Note that you \emph{must} append a ``.'' to denote it a real. The
  vector specified need not be normalized. Also, if you want it to
  be constrained along the $x$-$y$ vector you may do
  \begin{fdfexample}
    %block Geometry.Constraints
      Z 6
      Z 1 1. 1. 0.
    %endblock
  \end{fdfexample}

  Therefore the directional constraint will remove the force
  components that projects onto the direction specified.


\end{fdfentry}
