
A fast way to test your installation of \siesta\ and get a feeling for
the workings of the program is implemented in directory
\shell{Tests}\index{Tests}.
Assuming that you have built \siesta\ in \program{\_build}, you can
do

\begin{shellexample}
  cd _build
  ctest -L simple
\end{shellexample}
%

to test the execution of an assortment of tests. Executing
\program{ctest} with no other options will run all possible tests.
Output verification is also available via the
\program{VERIFY\_TESTS} environment variable:

\begin{shellexample}
  VERIFY_TESTS=1 ctest -L simple
\end{shellexample}

Other examples are provided in the \shell{Examples} directory.

Further information about the running of \siesta\, with tutorials and
how-to's on various topics, including the generation of
pseudopotentials with the \program{ATOM} code, can be found in the
documentation site \url{https://docs.siesta-project.org}.

\subsection{Specific execution options}

\siesta\ may be executed in different forms. The basic execution form
is
\begin{shellexample}
  siesta < RUN.fdf > RUN.out
\end{shellexample}
which uses a \emph{pipe} statement.
%
\siesta\ 4.1 and later does not require one to pipe in the input file
and the input file may instead be specified on the command line:
\begin{shellexample}
  siesta RUN.fdf > RUN.out
\end{shellexample}
\siesta\ 4.1 and later also accepts special flags and options
described in what follows:
\begin{itemize}
\item
All flags must start by one or more dashes (\shell{-}).
The number of leading dashes is irrelevant,
as long as there is at least one of them.
\item
Some flags (e.g., \shell[-L])
must be followed by a properly formed option string.
Other flags (e.g., \shell[-elec])
are logical toggles and they are not followed by option strings.
\item
Flags and option strings must all be separated by spaces
(and only spaces are valid separators for this).
\item
Option strings may be quoted.
Option strings that contain spaces need to either be quoted
or have the spaces replaced by a colon (\shell{:}) or by
an equal sign (\shell{=}).
\item
If the input file is not piped in, it can be given as an argument:
\begin{shellexample}
  siesta -L Hello -V 0.25:eV RUN.fdf > RUN.out
  siesta -L Hello RUN.fdf -V 0.25:eV > RUN.out
\end{shellexample}
\end{itemize}

The list of available flags and options is:
\begin{fdfoptions}

  \option[-help|-h]%
  \fdfindex*{Command line options:-h}%
  Print a help instruction and quit.

  \option[-version|-v]%
  \fdfindex*{Command line options:-v}%
  Print version information and quit.

  \option[-out|-o]%
  \fdfindex*{Command line options:-out}%
  \fdfindex*{Command line options:-o}%
  Specify the output file (instead of printing to the terminal).
  Example:

  \begin{shellexample}
  siesta --out RUN.out
  \end{shellexample}

  \option[-L]%
  \fdfindex*{Command line options:-L}%
  Override, temporarily, the \fdf{SystemLabel} flag.
  Example:

  \begin{shellexample}
  siesta -L Hello
  \end{shellexample}

  \option[-electrode|-elec]%
  \fdfindex*{Command line options:-electrode}%
  \fdfindex*{Command line options:-elec}%
  \fdfoverwrite{TS!HS.Save,TS!DE.Save}
  Denote this as an electrode calculation which forces the
  \sysfile{TSHS} and \sysfile{TSDE} files to be saved.

  \note This is equivalent to specifying \fdf{TS!HS.Save:true} and
  \fdf{TS!DE.Save:true} in the input file.

  \option[-V]%
  \fdfindex*{Command line options:-V}%
  \fdfoverwrite{TS!Voltage}
  Specify the bias for the current \tsiesta\ run.
  If no units are specified, $\mathrm{eV}$ are assumed.
  Example: any of the following three commands set the applied bias to
  $0.25\,\mathrm{eV}$:

  \begin{shellexample}
  siesta -V 0.25:eV
  siesta -V "0.25 eV"
  siesta -V 0.25
  \end{shellexample}

  \note This is equivalent to specifying \fdf{TS!Voltage} in the input
  file.

  \option[-fdf]%
  \fdfindex*{Command line options:-fdf}%
  Specify any FDF option string.  For example, another way to specify the bias
  of the example of the previous option would be:

  \begin{shellexample}
  siesta --fdf TS.Voltage=0.25:eV
  \end{shellexample}

\end{fdfoptions}
