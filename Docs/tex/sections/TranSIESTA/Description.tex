The \tsiesta\ method is a procedure to solve the electronic
structure of an open system formed by a finite structure sandwiched
between semi-infinite metallic leads. A finite bias can be applied
between leads, to drive a finite current. The method is described
in detail in \citet{Brandbyge2002,Papior2017}. In practical terms,
calculations using \tsiesta\ involve the solution of the
electronic density from the DFT Hamiltonian using Greens functions
techniques, instead of the usual diagonalization procedure. Therefore,
\tsiesta\ calculations involve a \siesta\ run, in which a
set of routines are invoked to solve the Greens functions and the
charge density for the open system. These routines are packed in a set
of modules, and we will refer to it as the '\tsiesta\ module'
in what follows.

\tsiesta\ was originally developed by Mads Brandbyge, Jos\'e-Luis
Mozos, Pablo Ordej\'on, Jeremy Taylor and Kurt
Stokbro\cite{Brandbyge2002}. It consisted, mainly, in setting up an
interface between \siesta\ and the (tight-binding) transport codes
developed by M. Brandbyge and K. Stokbro. Initially everything was
written in Fortran-77. As \siesta\ started to be translated to
Fortran-90, so were the \tsiesta\ parts of the code. This was
accomplished by Jos\'e-Luis Mozos, who also worked on the
parallelization of \tsiesta.
%
Subsequently Frederico D. Novaes extended \tsiesta\ to allow $k$-point
sampling for transverse directions. Additional extensions was
added by Nick R. Papior during 2012.

The current \tsiesta\ module has been completely rewritten by Nick
R. Papior and encompass highly advanced inversion algorithms as well
as allowing $N\geq1$ electrode setups among many new
features. Furthermore, the utility \tbtrans\ has also been fully
re-coded (by Nick R. Papior) to be a generic tight-binding code
capable of analyzing physics from the Greens function perspective in
$N\ge1$ setups\cite{Papior2017}.


\begin{itemize}
  \item%
  Transport calculations involve \emph{electrode} (EL) calculations,
  and subsequently the Scattering Region (SR) calculation. The
  \emph{electrode} calculations are usual \siesta\ calculations, but
  where files \sysfile{TSHS}, and optionally \sysfile{TSDE}, are
  generated. These files contain the information necessary for
  calculation of the self-energies. If any electrodes have identical
  structures (see below) the same files can and should be used to
  describe those. In general, however, electrodes can be different and
  therefore two different \sysfile{TSHS} files must be generated. The
  location of these electrode files must be specified in the \fdflib\
  input file of the SR calculation, see \fdf{TS!Elec.<>!HS}.

  \item %
  For the SR, \tsiesta\ starts with the usual \siesta\ procedure,
  converging a Density Matrix (DM) with the usual Kohn-Sham scheme for
  periodic systems. It uses this solution as an initial input for the
  Greens function self consistent cycle. Effectively you will start a
  \tsiesta\ calculation from a fully periodic calculation. This is why
  the $0\, V$ calculation should be the only calculation where you start
  from \siesta.

  \tsiesta\ stores the SCF DM in a file named \sysfile{TSDE}. In a rerun of
  the same system (meaning the same \fdf{SystemLabel}), if the
  code finds a \sysfile{TSDE} file in the directory, it will take this
  DM as the initial input and this is then considered a continuation
  run. In this case it does not perform an initial \siesta\ run. It
  must be clear that when starting a calculation from scratch, in the
  end one will find both files, \sysfile{DM} and \sysfile{TSDE}.
  %
  The first one stores the \siesta\ density matrix (periodic boundary
  conditions in all directions and no voltage), and the latter the
  \tsiesta\ solution.

  \item %
  When performing several bias calculations, it is heavily advised to
  run different bias' in different directories. To drastically improve
  convergence (and throughput) one should copy the \sysfile{TSDE} from
  the closest, previously, calculated bias to the current bias.

  \item %
  The \sysfile{TSDE} may be read equivalently as the
  \sysfile{DM}. Thus, it may be used by fx. \program{denchar} to
  analyze the non-equilibrium charge density. Alternatively one can
  use \sisl\cite{sisl} to interpolate the DM and EDM to speed up
  convergence.

  \item %
  As in the case of \siesta\ calculations, what \tsiesta\ does is to
  obtain a converged DM, but for open boundary conditions and possibly
  a finite bias applied between electrodes. The corresponding
  Hamiltonian matrix (once self consistency is achieved) of the SR is
  also stored in a \sysfile{TSHS} file. Subsequently, transport
  properties are obtained in a post-processing procedure using the
  \tbtrans\ code (located in the \program{Util/TS/TBtrans}
  directory). We note that the \sysfile{TSHS} files contain all the
  needed structural information (atomic positions, matrix elements,
  \ldots), and so the input (fdf) flags for the geometry and basis
  have no influence of the subsequent \tbtrans\ calculations.

  \item %
  When the non-equilibrium calculation uses different electrodes one
  should use so-called \emph{buffer} atoms behind the electrodes to act
  as additional screening regions when calculating the initial guess
  (using \siesta) for \tsiesta. Essentially they may be used to
  achieve a better ``bulk-like'' environment at the electrodes in the
  SR calculation.


  \item%
  An important parameter is the lower bound of the energy contours. It
  is a good practice, to start with a \siesta\ calculation for the SR
  and look at the eigenvalues of the system. The lower bound of the
  contours must be \emph{well} below the lowest eigenvalue.

  \item%
  Periodic boundary conditions are assumed in 2 cases.

  \begin{enumerate}
    \item For $\Nelec\neq 2$ all lattice vectors are periodic, users
    \emph{must} manually define \fdf{TS!kgrid!MonkhorstPack}

    \item For $\Nelec=2$ \tsiesta\ will auto-detect if both electrodes
    are semi-infinite along the same lattice vector. If so, only 1 $k$
    point will be used along that lattice vector.
  \end{enumerate}

  \item%
  The default algorithm for matrix inversion is the BTD method, before
  starting a \tsiesta\ calculation please run with the analyzation
  step \fdf{TS!Analyze} (note this is very fast and can be done on any
  desktop computer, regardless of system size).

  \item%
  Importantly(!) the $k$-point sampling need typically be much higher
  in a \tbtrans\ calculation to achieve a converged transmission
  function.

  \item%
  Energies from \tsiesta\ are \emph{not} to be trusted since the open
  boundaries complicates the energy calculation. Therefore care needs
  to be taken when comparing energies between different calculations
  and/or different bias'.

  \item%
  Always ensure that charges are preserved in the scattering region
  calculation. Doing the SCF an output like the following will be shown:
  \begin{output}[fontsize=\footnotesize]
ts-q:         D        E1        C1        E2        C2        dQ
ts-q:   436.147   392.146     3.871   392.146     3.871  7.996E-3
  \end{output}
  Always ensure the last column (\code{dQ}) is a very small fraction of
  the total number of electrons. Ideally this should be $0$.
  %
  For $0$ bias calculations this should be very small, typically less
  than $0.1\,\%$ of the total charge in the system. If this is not the
  case, it probably means that there is not enough screening towards the
  electrodes which can be solved by adding more electrode layers between
  the electrode and the scattering region. This layer thickness is
  \emph{very} important to obtain a correct open boundary calculation.

  \item%
  Do \emph{not} perform \tsiesta\ calculations using semi-conducting
  electrodes. The basic premise of \tsiesta\ calculations is that the
  electrodes \emph{behave like bulk} in the electrode regions of the
  SR. This means that the distance between the electrode and the
  perturbed must equal the screening length of the electrode.

  This is problematic for semi-conducting systems since they
  intrinsically have a very long screening length.

  In addition, the Fermi-level of semi-conductors are not well-defined
  since it may be placed anywhere in the band gap.

\end{itemize}
