
The options for $k$-point sampling are identical to the \siesta\
options, \fdf{kgrid!MonkhorstPack}, \fdf{kgrid!Cutoff} or
\fdf{kgrid!File}.

One may however use specific \tsiesta\ $k$-points by using these
options:

\begin{fdfentry}{TS.kgrid!MonkhorstPack}[block]<\fdfvalue{kgrid!MonkhorstPack}>%

  See \fdf{kgrid!MonkhorstPack} for details.

\end{fdfentry}

\begin{fdfentry}{TS.kgrid!Cutoff}[length]<$0.\,\mathrm{Bohr}$>

  See \fdf{kgrid!Cutoff} for details.

\end{fdfentry}

\begin{fdfentry}{TS.kgrid!File}[string]<none>

  See \fdf{kgrid!File} for details.

\end{fdfentry}


\subsubsection{Algorithm specific options}

These options adhere to the specific solution methods available for
\tsiesta. For instance the \fdf*{TS.BTD.*} options adhere only when
using \fdf{TS!SolutionMethod:BTD}, similarly for options with
\fdf*{MUMPS}.

\begin{fdfentry}{TS!BTD!Pivot}[string]<\nonvalue{first electrode}>

  Decide on the partitioning for the BTD matrix. One may denote either
  \fdf*{atom+} or \fdf*{orb+} as a prefix which does the analysis on
  the atomic sparsity pattern or the full orbital sparsity pattern,
  respectively. If neither are used it will default to \fdf*{atom+}.

  Please see \fdf{TS!Analyze}.

  \begin{fdfoptions}

    \option[<elec-name>|CG-<elec-name>]%
    The partitioning will be a connectivity graph starting from the
    electrode denoted by the name. This name \emph{must} be found in
    the \fdf{TS!Elecs} block. One can append more than one electrode
    to simultaneously start from more than 1 electrode. This may be
    necessary for multi-terminal calculations.

    \option[rev-CM] %
    Use the reverse Cuthill-McKee for pivoting the matrix elements to
    reduce bandwidth. One may omit \fdf*{rev-} to use the standard
    Cuthill-McKee algorithm (not recommended).

    This pivoting scheme depends on the initial starting
    electrodes, append \fdf*{+<elec-name>} to start the Cuthill-McKee
    algorithm from the specified electrode(s).

    \option[GPS] %
    Use the Gibbs-Poole-Stockmeyer algorithm for reducing the
    bandwidth.

    \option[GGPS] %
    Use the generalized Gibbs-Poole-Stockmeyer algorithm for reducing
    the bandwidth.

    \note this algorithm does not work on dis-connected graphs.

    \option[PCG] %
    Use the perphiral connectivity graph algorithm for reducing the
    bandwidth.

    This pivoting scheme \emph{may} depend on the initial starting
    electrode(s), append \fdf*{+<elec-name>} to initialize the PCG
    algorithm from the specified electrode(s).

  \end{fdfoptions}

  Examples are
  \begin{fdfexample}
    TS.BTD.Pivot atom+GGPS
    TS.BTD.Pivot GGPS
    TS.BTD.Pivot orb+GGPS
    TS.BTD.Pivot orb+PCG+Left
  \end{fdfexample}
  where the first two are equivalent. The 3rd and 4th are more heavy
  on analysis and will typically not improve the bandwidth reduction.

\end{fdfentry}

\begin{fdfentry}{TS!BTD!Optimize}[string]<speed|memory>

  When selecting the smallest blocks for the BTD matrix there are
  certain criteria that may change the size of each block. For very
  memory consuming jobs one may choose the \fdf*{memory}.

  \note often both methods provide \emph{exactly} the same BTD matrix
  due to constraints on the matrix.

\end{fdfentry}

\begin{fdfentry}{TS!BTD!Guess1.Min}[int]<\nonvalue{empirically determined}>
  \fdfdepend{TS!BTD!Guess1.Max}

  Constructing the blocks for the BTD starts by \emph{guessing} the
  first block size. One could guess on all different block sizes, but
  to speed up the process one can define a smaller range of guesses by
  defining \fdf{TS!BTD!Guess1.Min} and \fdf{TS!BTD!Guess1.Max}.

  The initial guessed block size will be between the two values.

  By default this is $1/4$ of the minimum bandwidth for a selected
  first set of orbitals.

  \note setting this to 1 may sometimes improve the final BTD matrix
  blocks.

\end{fdfentry}

\begin{fdfentry}{TS!BTD!Guess1.Max}[int]<\nonvalue{empirically determined}>
  \fdfdepend{TS!BTD!Guess1.Min}

  See \fdf{TS!BTD!Guess1.Min}.

  \note for improved initialization performance setting Min/Max flags
  to the first block size for a given pivoting scheme will drastically
  reduce the search space and make initialization much
  faster.

\end{fdfentry}

\begin{fdfentry}{TS!BTD!Spectral}[string]<propagation|column>

  How to compute the spectral function ($G\Gamma G^\dagger$).

  For $\Nelec<4$ this defaults to \fdf*{propagation} which should be the
  fastest.

  For $\Nelec\ge4$ this defaults to \fdf*{column}.

  Check which has the best performance for your system if you endeavor
  on huge amounts of calculations for the same system.

\end{fdfentry}


\begin{fdfentry}{TS!MUMPS!Ordering}[string]<\nonvalue{read MUMPS
      manual}>

  One may select from a number of different matrix orderings which are
  all described in the MUMPS manual.

  The following list of orderings are available (without detailing
  their differences): %
  \fdf*{auto}, \fdf*{AMD}, \fdf*{AMF}, \fdf*{SCOTCH}, \fdf*{PORD},
  \fdf*{METIS}, \fdf*{QAMD}.

\end{fdfentry}

\begin{fdfentry}{TS!MUMPS!Memory}[integer]<20>

  Specify a factor for the memory consumption in MUMPS. See the
  \fdf*{INFOG(9)} entry in the MUMPS manual. Generally if \tsiesta\
  dies and \fdf*{INFOG(9)=-9} one should increase this number.

\end{fdfentry}

\begin{fdfentry}{TS!MUMPS!BlockingFactor}[integer]<112>

  Specify the number of internal block sizes. Larger numbers increases
  performance at the cost of memory.

  \note this option may heavily influence performance.

\end{fdfentry}

\subsubsection{Poisson solution for fixed boundary conditions}

\tsiesta\ requires fixed boundary conditions and forcing this is an
intricate and important detail.

It is important that these options are exactly the same if one reuses
the \sysfile{TSDE} files.

\begin{fdfentry}{TS!Poisson}[string]<ramp|elec-box|\nonvalue{file}>

  Define how the correction of the Poisson equation is
  superimposed. The default is to apply the linear correction across
  the entire cell (if there are two semi-infinite aligned
  electrodes). Otherwise this defaults to the \emph{box} solution
  which will introduce spurious effects at the electrode
  boundaries. In this case you are encouraged to supply a \fdf*{file}.

  If the input is a \fdf*{file}, it should be a NetCDF file containing
  the grid information which acts as the boundary conditions for the
  SCF cycle.
  The grid information should conform to the grid size of the
  unit-cell in the simulation.
  %
  \note the file option is only applicable if compiled with CDF4
  compliance.

  \begin{fdfoptions}
    \option[ramp]%
    \fdfindex*{TS!Poisson:ramp}%

    Apply the ramp for the full cell. This is the default for 2
    electrodes.

    \option[<file>]%
    \fdfindex*{TS!Poisson:<file>}%

    Specify an external file used as the boundary conditions for the
    applied bias. This is encouraged to use for $\Nelec>2$ electrode
    calculations but may also be used when an \emph{a priori}
    potential profile is know.

    The file should contain something similar to this output
    (\code{ncdump -h}):
    \begin{output}[fontsize=\footnotesize]
netcdf <file> {
dimensions:
	one = 1 ;
	a = 43 ;
	b = 451 ;
	c = 350 ;
variables:
	double Vmin(one) ;
		Vmin:unit = "Ry" ;
	double Vmax(one) ;
		Vmax:unit = "Ry" ;
	double V(c, b, a) ;
		V:unit = "Ry" ;
}
    \end{output}
    Note that the units should be in Ry. \code{Vmax}/\code{Vmin}
    should contain the maximum/minimum fixed boundary conditions in
    the Poisson solution. This is used internally by \tsiesta\ to
    scale the potential to arbitrary $V$. This enables the Poisson
    solution to only be solved \emph{once} independent on subsequent
    calculations. For chemical potential configurations where the
    Poisson solution is not linearly dependent one have to create
    separate files for each applied bias.


    \option[elec-box]%
    \fdfindex*{TS!Poisson:elec-box}%

    The default potential profile for $\Nelec>2$, or when the electrodes
    does are not aligned (in terms of their transport direction).

    \note usage of this Poisson solution is \emph{highly}
    discouraged. Please see \fdf{TS!Poisson:<file>}.

  \end{fdfoptions}

\end{fdfentry}

\begin{fdfentry}{TS!Hartree.Fix}[string]<[-+][ABC]>

  Specify which plane to fix the Hartree potential at. For regular (2
  electrode calculations with a single transport direction) this
  should not be set.
  %
  For $\Nelec\neq2$ electrode systems one \emph{have} to specify a
  plane to fix. One can specify one or several planes to fix. Users
  are encouraged to fix the plane where the entire plane has the
  highest/lowest potential.

\end{fdfentry}

\begin{fdfentry}{TS!Hartree.Fix!Frac}[real]<$1.$>

  Fraction of the correction that is applied.

  \note this is an experimental feature!

\end{fdfentry}

\begin{fdfentry}{TS!Hartree.Offset}[energy]<$0\,\mathrm{eV}$>

  An offset in the Hartree potential to match the electrode potential.

  This value may be useful in certain cases where the Hartree
  potentials are very different between the electrode and device
  region calculations.

  This should not be changed between different bias calculations. It
  directly relates to the reference energy level ($E_F$).

\end{fdfentry}

\subsubsection{Electrode description options}

As \tsiesta\ supports $\Nelec$ electrodes one needs to specify all
electrodes in a generic input format.

Note that electrodes should be \emph{metallic} so that the Fermi-level
is well defined. Please see Sec.~\ref{sec:transiesta:description} for
more details.

\begin{fdfentry}{TS!Elecs}[block]

  Each line denote an electrode which is queried in \fdf{TS!Elec.<>}
  for its setup.

\end{fdfentry}

\begin{fdfentry}{TS!Elec.<>}[block]

  Each line represents a setting for electrode \fdf*{<>}.
  There are a few lines that \emph{must} be present, \fdf*{HS},
  \fdf*{semi-inf-dir}, \fdf*{electrode-pos}, \fdf*{chem-pot}. The
  remaining options are optional.

  \note Options prefixed with \fdf*{tbt} are neglected in \tsiesta\
  calculations. In \tbtrans\ calculations these flags has precedence
  over the other options and \emph{must} be placed at the end of the
  block.

  \begin{fdfoptions}

    \option[HS]%
    \fdfindex*{TS!Elec.<>!HS}%
    The Hamiltonian information from the initial electrode
    calculation. This file retains the geometrical information as well
    as the Hamiltonian, overlap matrix and the Fermi-level of the
    electrode.
    %
    This is a file-path and the electrode \sysfile{TSHS} need not be
    located in the simulation folder.

    \note Please note that \tsiesta\ expects a metallic electrode.
    Results can not be trusted for semi-conductors.

    \option[semi-inf-direction|semi-inf-dir|semi-inf]%
    \fdfindex*{TS!Elec.<>!semi-inf-direction}%
    The semi-infinite direction of the electrode with respect to the
    electrode unit-cell.

    It may be one of \fdf*{[-+][abc]}, \fdf*{[-+]A[123]}, \fdf*{ab},
    \fdf*{ac}, \fdf*{bc} or \fdf*{abc}. The latter four all refer to a
    real-space self-energy as described in \cite{Papior2019}.

    \note this direction is \emph{not} with respect to the scattering
    region unit cell. It is with respect to the electrode unit
    cell. \tsiesta\ will figure out the alignment of the electrode
    unit cell and the scattering region unit-cell.

    \option[chemical-potential|chem-pot|mu]%
    \fdfindex*{TS!Elec.<>!chemical-potential}%
    The chemical potential that is associated with this
    electrode. This is a string that should be present in the
    \fdf{TS!ChemPots} block.

    \option[electrode-position|elec-pos]%
    \fdfindex*{TS!Elec.<>!electrode-position}%
    The index of the electrode in the scattering region.
    This may be given by either \fdf*{elec-pos <idx>}, which refers to
    the first atomic index of the electrode residing at index
    \fdf*{<idx>}. Else the electrode position may be given via
    \fdf*{elec-pos end <idx>} where the last index of the electrode
    will be located at \fdf*{<idx>}.

    \option[used-atoms]%
    \fdfindex*{TS!Elec.<>!used-atoms}%
    \fdfdepend{TS!Elec.<>!semi-inf-direction}%
    Number of atoms from the electrode calculation that is used in the
    scattering region as electrode. This may be useful when the
    periodicity of the electrodes forces extensive electrodes in the
    semi-infinite direction.

    If the semi-infinite direction is \emph{positive}, the first atoms will
    be retained.
    Contrary, if the semi-infinite direction is \emph{negative}, the last
    atoms will be retained.

    \note do not set this if you use all atoms in the electrode.

    \option[Bulk]%
    \fdfindex*{TS!Elec.<>!Bulk}%
    \fdfindex*{TS!Elec.<>!Bulk:true}%
    \fdfindex*{TS!Elec.<>!Bulk:false}%
    Control whether the Hamiltonian of the electrode region in the
    scattering region is enforced \emph{bulk} or whether the
    Hamiltonian is taken from the scattering region elements.

    This defaults to \fdftrue. If there are buffer atoms \emph{behind}
    the electrode it may be advantageous to set this to false to
    extend the electrode region, otherwise it is recommended to keep
    the default.

    This option changes how $\mathbf M_{\elec,\elec}$, see Eq.~\eqref{eq:negf:green-elec}, is setup.

    For \fdftrue\ $\big\{\Ham_\elec, \SO_\elec\big\}$ are taken from the
    electrode file (\fdf{TS!Elec.<>!HS}).

    For \fdffalse\ $\big\{\Ham_\elec, \SO_\elec\big\}$ are substituted by
    the device calculations electrode region.
    I.e. it is the self-consistent Hamiltonian.

    \option[DM-update]%
    \fdfindex*{TS!Elec.<>!DM-update}%
    \fdfindex*{TS!Elec.<>!DM-update:none}%
    \fdfindex*{TS!Elec.<>!DM-update:all}%
    \fdfdepend{TS!Elec.<>!Bulk}%
    String of values \fdf*{none}, \fdf*{cross-terms} or \fdf*{all}
    which controls which part of the electrode density matrix elements
    that are updated.
    %
    The density matrices that comprises an electrode and
    device-electrode region can be written as (omitting the central
    device region)
    \begin{equation}
      \label{eq:ts-dm-update}
      \DM =
      \begin{bmatrix}
        \DM_{\mathfrak e} & \DM_{\mathfrak eD} & 0
        \\
        \DM_{D\mathfrak e} & \ddots & \ddots
        \\
        0  & \ddots
      \end{bmatrix}
    \end{equation}
    This flag determines whether $\DM_{\mathfrak e}$ (\fdf*{all}) or
    $\DM_{\mathfrak eD}$ (\fdf*{cross-terms} and \fdf*{all}) or
    neither (\fdf*{none}) are updated in the SCF. The density matrices
    contains the charges and thus affects the Hamiltonian and Poisson
    solutions. Generally the default value will suffice and is
    recommended.

    If \fdf{TS!Elec.<>!Bulk:false} this is forced to \fdf*{all} and
    cannot be changed.

    If \fdf{TS!Elec.<>!Bulk:true} this defaults to \fdf*{cross-terms},
    but may be changed.

    \note if this is \fdf*{none} the forces on the atoms coupled to
    the electrode regions are \emph{not} to be trusted. The value
    \fdf*{none} should be avoided, if possible.

    \option[DM-init]%
    \fdfindex*{TS!Elec.<>!DM-init}%
    \fdfindex*{TS!Elec.<>!DM-init:diagon}%
    \fdfindex*{TS!Elec.<>!DM-init:bulk}%
    \fdfdepend{TS!Elecs!DM.Init,TS!Elec.<>!Bulk,TS!Voltage}%
    String of values \fdf*{bulk}, \fdf*{diagon} (default) or
    \fdf*{force-bulk} which controls whether the DM is initially
    overwritten by the DM from the bulk electrode calculation. This
    requires the DM file for the electrode to be present. Only
    \fdf*{force-bulk} will have effect if $V\neq0$. Otherwise this
    option only affects $V=0$ calculations.

    The density matrix elements in the electrodes of the scattering
    region may be forcefully set to the bulk values by reading in the
    DM of the corresponding electrode. If one uses
    \fdf{TS!Elec.<>!Bulk:false} it may be dis-advantageous to set this
    to \fdf*{bulk}.
    If the system is well setup (good screening towards electrodes),
    setting this to \fdf*{bulk} may be advantageous.

    This option may be used to check how good the electrodes are
    screened, see \fdf{TS!dQ:fermi}.

    \option[out-of-core]%
    \fdfindex*{TS!Elec.<>!Out-of-core}%
    \fdfdepend{TS!Elec.<>!Gf}%
    If \fdftrue\ (default) the GF files are created which contain
    the surface Green function.
    If \fdffalse\ the surface Green function will be calculated when
    needed.
    Setting this to \fdffalse\ will heavily degrade performance and
    it is highly discouraged!

    \option[Gf]%
    \fdfindex*{TS!Elec.<>!Gf}%
    String with filename of the surface Green function data
    (\sysfile{TSGF*}). This may be used to place a common surface
    Green function file in a top directory which may then be used in
    all calculations using the same electrode and the same contour.
    %
    If doing many calculations with the same electrode and $\mathbf k$, $E$ grids,
    then this can greatly improve throughput. It has a minor cost of disk-space.
    Note that the energy-grids are dependent on the applied bias.

    \option[Gf-Reuse]%
    \fdfindex*{TS!Elec.<>!Gf-Reuse}%
    \fdfdepend{TS!Elec.<>!Out-of-core,TS!Elec.<>!Gf}%
    Logical deciding whether the surface Green function file should be
    re-used or deleted.
    %
    If this is \fdffalse\ the surface Green function file is deleted
    and re-created upon start.

    \option[pre-expand]%
    \fdfindex*{TS!Elec.<>!pre-expand}%
    \fdfdepend{TS!Elec.<>!out-of-core}%
    String denoting how the expansion of the surface Green function
    file will be performed. This only affects the Green function file
    if \fdf*{Bloch} is larger than 1. By default the Green function
    file will contain the fully expanded surface Green function, but
    not Hamiltonian and overlap matrices (\fdf*{Green}). One may
    reduce the file size by setting this to \fdf*{Green} which only
    expands the surface Green function. Finally \fdf*{none} may be
    passed to reduce the file size to the bare minimum.
    %
    For performance reasons \fdf*{all} is preferred.

    If disk-space is a limited resource and the \sysfile{TSGF*} files
    are really big, try \fdf*{none}.

    \option[Eta]%
    \fdfindex*{TS!Elec.<>!Eta}%
    \fdfdepend{TS!Elecs!Eta}%
    Control the imaginary energy ($\eta$) of the surface Green
    function for this electrode.

    The imaginary part is \emph{only} used in the non-equilibrium
    contours since the equilibrium are already lifted into the complex
    plane. Thus this $\eta$ reflects the imaginary part in the
    $G\Gamma G^\dagger$ calculations. Ensure that all imaginary values
    are larger than $0$ as otherwise \tsiesta\ may seg-fault.

    \note if this energy is negative the complex value associated with
    the non-equilibrium contour is used. This is particularly useful
    when providing a user-defined contour along the real axis.

    See Sec.~\ref{sec:negf-equations} for details.
    This options changes the $\eta$ value in the calculated self-energy
    ($\SE(E+i\eta)$), while it does not change the $\eta$ value used
    in the device region.

    \option[DE]%
    \fdfindex*{TS!Elec.<>!DE}%
    \fdfdepend{TS!Elec.<>!DM-init}%
    Density and energy density matrix file for the electrode. This may
    be used to initialize the density matrix elements in the electrode
    region by the bulk values. See \fdf{TS!Elec.<>!DM-init:bulk}.

    \note this should only be performed on one \tsiesta\ calculation
    as then the scattering region \sysfile{TSDE} contains the
    electrode density matrix.

    \option[Bloch]%
    \fdfindex*{TS!Elec.<>!Bloch}%
    $3$ integers should be present on this line which each denote the
    number of times bigger the scattering region electrode is compared
    to the electrode, in each lattice direction. Remark that these
    expansion coefficients are with regard to the electrode unit-cell.
    This is denoted ``Bloch'' because it is an expansion based on
    Bloch waves.

    \note Using symmetries such as periodicity will greatly increase
    performance.

    \option[Bloch-A/a1|B/a2|C/a3]%
    \fdfindex{TS!Elec.<>!Bloch}%
    Specific Bloch expansions in each of the electrode unit-cell
    direction. See \fdf*{Bloch} for details.

    \option[Accuracy]%
    \fdfindex*{TS!Elec.<>!Accuracy}%
    \fdfdepend{TS!Elecs!Accuracy}%
    Control the convergence accuracy required for the self-energy
    calculation when using the Lopez-Sancho, Lopez-Sancho iterative
    scheme.

    \note advanced use \emph{only}.

    \option[delta-Ef]%
    \fdfindex*{TS!Elec.<>!delta-Ef}%
    Specify an offset for the Fermi-level of the electrode. This will
    directly be added to the Fermi-level found in the electrode file.

    Effectively this will transform the used chemical potential to
    \begin{equation}
      \mu'_{\mathrm{used}} = \mu_{\mathrm{used}} + \delta E_F.
    \end{equation}

    \note this option only makes sense for semi-conducting electrodes
    since it shifts the entire electronic structure. This is because
    the Fermi-level may be arbitrarily placed anywhere in the band
    gap. It is the users responsibility to define a value which does
    not introduce a potential drop between the electrode and device
    region. Please do not use unless you really know what you are
    doing.

    \option[V-fraction]%
    \fdfindex*{TS!Elec.<>!V-fraction}%

    Specify the fraction of the chemical potential shift in the
    electrode-device coupling region. This corresponds to altering Eq.~\eqref{eq:negf:green-elec} by:
    \begin{equation}
      \Ham_{\elec,D} \leftarrow \Ham_{\elec,D} +
      \mu_{\elec} \mathrm{V-fraction} \SO_{\elec,D}
    \end{equation}
    in the coupling region. Consequently the value \emph{must} be
    between $0$ and $1$.

    \note this option \emph{only} makes sense for
    \fdf{TS!Elec.<>!DM-update:none} since otherwise the electrostatic
    potential will be incorporated in the Hamiltonian.

    Only expert users should play with this number.

    \option[check-kgrid]%
    \fdfindex*{TS!Elec.<>!check-kgrid}%
    For $\Nelec$ electrode calculations the $\mathbf k$ mesh will sometimes
    not be equivalent for the electrodes and the device region
    calculations. However, \tsiesta\ requires that the device and
    electrode $\mathbf k$ samplings are commensurate. This flag
    controls whether this check is enforced for a given electrode.

    \note only use if fully aware of the implications!

  \end{fdfoptions}

\end{fdfentry}

There are several flags which are globally controlling the variables
for the electrodes (with \fdf{TS!Elec.<>} taking precedence).

\begin{fdflogicalT}{TS!Elecs!Bulk}

  This globally controls how the Hamiltonian is treated in all
  electrodes.
  %
  See \fdf{TS!Elec.<>!Bulk}.

\end{fdflogicalT}

\begin{fdfentry}{TS!Elecs!Eta}[energy]<$1\,\mathrm{meV}$>

  Globally control the imaginary energy ($\eta$) used for the surface
  Green function calculation on the non-equilibrium contour.
  %
  See \fdf{TS!Elec.<>!Eta} for extended details on the usage of this
  flag.

\end{fdfentry}

\begin{fdfentry}{TS!Elecs!Accuracy}[energy]<$10^{-13}\,\mathrm{eV}$>

  Globally control the accuracy required for convergence of the self-energy.
  %
  See \fdf{TS!Elec.<>!Accuracy}.

\end{fdfentry}

\begin{fdflogicalF}{TS!Elecs!Neglect.Principal}

  If this is \fdffalse\ \tsiesta\ dies if there are connections beyond
  the principal cell.

  \note set this to \fdftrue\ with care, non-physical results may
  arise. Use at your own risk!

\end{fdflogicalF}

\begin{fdflogicalT}{TS!Elecs!Gf.Reuse}

  Globally control whether the surface Green function files should
  be re-used (\fdftrue) or re-created (\fdffalse).

  See \fdf{TS!Elec.<>!Gf-Reuse}.

\end{fdflogicalT}

\begin{fdflogicalT}{TS!Elecs!Out-of-core}

  Whether the electrodes will calculate the self energy at each SCF
  step. Using this will not require the surface Green function files
  but at the cost of heavily degraded performance.

  See \fdf{TS!Elec.<>!Out-of-core}.

\end{fdflogicalT}

\begin{fdfentry}{TS!Elecs!DM.Update}[string]<cross-terms|all|none>

  Globally controls which parts of the electrode density matrix
  gets updated.

  See \fdf{TS!Elec.<>!DM-update}.

\end{fdfentry}

\begin{fdfentry}{TS!Elecs!DM.Init}[string]<diagon|bulk|force-bulk>
  \fdfindex*{TS!Elecs!DM.Init:bulk}%
  \fdfindex*{TS!Elecs!DM.Init:diagon}%

  Specify how the density matrix elements in the electrode regions of
  the scattering region will be initialized when starting \tsiesta.

  See \fdf{TS!Elec.<>!DM-init}.

\end{fdfentry}

\begin{fdfentry}{TS!Elecs!Coord.EPS}[length]<$0.001\,\mathrm{Ang}$>

  When using Bloch expansion of the self-energies one may experience
  difficulties in obtaining perfectly aligned electrode coordinates.

  This parameter controls how strict the criteria for equivalent
  atomic coordinates is. If \tsiesta\ crashes due to mismatch between
  the electrode atomic coordinates and the scattering region
  calculation, one may increase this criteria. This should only be
  done if one is sure that the atomic coordinates are almost similar
  and that the difference in electronic structures of the two may be
  negligible.

\end{fdfentry}


\subsubsection{Chemical potentials}
\label{sec:ts:chem-pot}

For $\Nelec$ electrodes there will also be $N_\mu$ chemical
potentials. They are defined via blocks similar to \fdf{TS!Elecs}.

\begin{fdfentry}{TS!ChemPots}[block]

  Each line denotes a new chemical potential which is defined in the
  \fdf{TS!ChemPot.<>} block.

\end{fdfentry}

\begin{fdfentry}{TS!ChemPot.<>}[block]

  Each line defines a setting for the chemical potential named
  \fdf*{<>}.

  \begin{fdfoptions}

    \option[chemical-shift|mu]%
    \fdfindex*{TS!ChemPot.<>!chemical-shift}%
    \fdfindex*{TS!ChemPot.<>!mu}%

    Define the chemical shift (an energy) for this chemical
    potential. One may specify the shift in terms of the applied bias
    using \fdf*{V/<integer>} instead of explicitly typing the energy.

    \option[contour.eq]%
    \fdfindex*{TS!ChemPot.<>!contour.eq}%
    A subblock which defines the integration curves for the
    equilibrium contour for this equilibrium chemical potential. One
    may supply as many different contours to create whatever shape of
    the contour

    Its format is
    \begin{fdfexample}
      contour.eq
       begin
        <contour-name-1>
        <contour-name-2>
        ...
       end
    \end{fdfexample}

    \note If you do \emph{not} specify \fdf*{contour.eq} in the block
    one will automatically use the continued fraction method and you
    are encouraged to use $50$ or more poles\cite{Ozaki2010}.

    \option[ElectronicTemperature|Temp|kT]%
    \fdfindex*{TS!ChemPot.<>!ElectronicTemperature}%
    \fdfindex*{TS!ChemPot.<>!Temp}%
    \fdfindex*{TS!ChemPot.<>!kT}%

    Specify the electronic temperature (as an energy or in
    Kelvin). This defaults to \fdf{TS!ElectronicTemperature}.

    One may specify this in units of \fdf{TS!ElectronicTemperature} by
    using the unit \fdf*{kT}.

    \option[contour.eq.pole]%
    \fdfindex*{TS!ChemPot.<>!contour.eq.pole}%

    Define the number of poles used via an energy
    specification. \tsiesta\ will automatically convert the energy to
    the closest number of poles (rounding up).

    \note this has precedence over
    \fdf{TS!ChemPot.<>!contour.eq.pole.N} if it is specified
    \emph{and} a positive energy. Set this to a negative energy to
    directly control the number of poles.

    \option[contour.eq.pole.N]%
    \fdfindex*{TS!ChemPot.<>!contour.eq.pole.N}%

    Define the number of poles via an integer.

    \note this will only take effect if
    \fdf{TS!ChemPot.<>!contour.eq.pole} is a negative energy.

  \end{fdfoptions}

  \note It is important to realize that the parametrization in 4.1 of
  the voltage into the chemical potentials enables one to have a
  \emph{single} input file which is never required to be changed, even
  when changing the applied bias (if using the command line options
  for specifying the applied bias).
  %
  This is different from 4.0 and prior versions since one had to
  manually change the \fdf*{TS.biasContour.NumPoints} for each applied
  bias.

\end{fdfentry}

These options complicate the input sequence for regular $2$ electrode
which is unfortunate.

Using \program{tselecs.sh -only-mu} yields this output:
\begin{fdfexample}
  %block TS.ChemPots
    Left
    Right
  %endblock
  %block TS.ChemPot.Left
    mu V/2
    contour.eq
      begin
        C-Left
        T-Left
      end
  %endblock
  %block TS.ChemPot.Right
    mu -V/2
    contour.eq
      begin
        C-Right
        T-Right
      end
  %endblock
\end{fdfexample}

Note that the default is a $2$ electrode setup with chemical
potentials associated directly with the electrode names
``Left''/``Right''. Each chemical potential has two parts of the
equilibrium contour named according to their name.



\subsubsection{Complex contour integration options}

Specifying the contour for $\Nelec$ electrode systems is a bit
extensive due to the possibility of more than 2 chemical
potentials. Please use the \program{Util/TS/tselecs.sh} as a means to
create default input blocks.

The contours are split in two segments. One, being the equilibrium
contour of each of the different chemical potentials. The second for
the non-equilibrium contour. The equilibrium contours are shifted
according to their chemical potentials with respect to a reference
energy. Note that for \tsiesta\ the reference energy is named the
Fermi-level, which is rather unfortunate (for non-equilibrium but not
equilibrium). Fortunately the non-equilibrium contours are defined
from different chemical potentials Fermi functions, and as such this
contour is defined in the window of the minimum and maximum chemical
potentials. Because the reference energy is the periodic Fermi level
it is advised to retain the average chemical potentials equal to
$0$. Otherwise applying different bias will shift transmission curves
calculated via \tbtrans\ relative to the average chemical potential.

In this section the equilibrium contours are defined, and in the next
section the non-equilibrium contours are defined.

\begin{fdfentry}{TS!Contours!Eq.Pole}[energy]<$1.5\,\mathrm{eV}$>

  The imaginary part of the line integral crossing the chemical
  potential. Note that the actual number of poles may differ between
  different calculations where the electronic temperatures are
  different.

  \note if the energy specified is negative,
  \fdf{TS!Contours!Eq.Pole.N} takes effect.

\end{fdfentry}

\begin{fdfentry}{TS!Contours!Eq.Pole.N}[integer]<8>

  Manually select the number poles for the equilibrium contour.

  \note this flag will only take effect if \fdf{TS!Contours!Eq.Pole}
  is a negative energy.

\end{fdfentry}

\begin{fdfentry}{TS!Contour.<>}[block]

  Specify a contour named \fdf*{<>} with options within the block.

  The names \fdf*{<>} are taken from the
  \fdf{TS!ChemPot.<>!contour.eq} block in the chemical potentials.

  The format of this block is made up of at least $4$ lines, in the
  following order of appearance.

  \begin{fdfoptions}

    \option[part]%
    \fdfindex*{TS!Contour.<>!part}%

    Specify which part of the equilibrium contour this is:
    \begin{fdfoptions}

      \option[circle]%
      The initial circular part of the contour

      \option[square]%
      The initial square part of the contour

      \option[line]%
      The straight line of the contour

      \option[tail]%
      The final part of the contour \emph{must} be a tail which
      denotes the Fermi function tail.

    \end{fdfoptions}

    \option[from \emph{a} to \emph{b}]%
    \fdfindex*{TS!Contour.<>!from}%

    Define the integration range on the energy axis.
    Thus \emph{a} and \emph{b} are energies.

    The parameters may also be given values \fdf*{prev}/\fdf*{next}
    which is the equivalent of specifying the same energy as the
    previous contour it is connected to.

    \note that \emph{b} may be supplied as \fdf*{inf} for \fdf*{tail}
    parts.

    \option[points/delta]%
    \fdfindex*{TS!Contour.<>!points}%
    \fdfindex*{TS!Contour.<>!delta}%

    Define the number of integration points/energy separation.
    If specifying the number of points an integer should be supplied.

    If specifying the separation between consecutive points an energy
    should be supplied.

    \option[method]%
    \fdfindex*{TS!Contour.<>!method}%

    Specify the numerical method used to conduct the integration. Here
    a number of different numerical integration schemes are accessible

    \begin{fdfoptions}
      \option[mid|mid-rule]%
      Use the mid-rule for integration.

      \option[simpson|simpson-mix]%
      Use the composite Simpson $3/8$ rule (three point Newton-Cotes).

      \option[boole|boole-mix]%
      Use the composite Booles rule (five point Newton-Cotes).

      \option[G-legendre]%
      Gauss-Legendre quadrature.

      \note has \fdf*{opt left}

      \note has \fdf*{opt right}

      \option[tanh-sinh]%
      Tanh-Sinh quadrature.

      \note has \fdf*{opt precision <>}

      \note has \fdf*{opt left}

      \note has \fdf*{opt right}

      \option[G-Fermi]%
      Gauss-Fermi quadrature (only on tails).

    \end{fdfoptions}

    \option[opt]%
    \fdfindex*{TS!Contour.<>!opt}%

    Specify additional options for the \fdf*{method}. Only a selected
    subset of the methods have additional options.

  \end{fdfoptions}

\end{fdfentry}

These options complicate the input sequence for regular $2$ electrode
which is unfortunate. However, it allows highly customizable contours.

Using \program{tselecs.sh -only-c} yields this output:
\begin{fdfexample}
  TS.Contours.Eq.Pole 2.5 eV
  %block TS.Contour.C-Left
    part circle
     from -40. eV + V/2 to -10 kT + V/2
       points 25
        method g-legendre
         opt right
  %endblock
  %block TS.Contour.T-Left
    part tail
     from prev to inf
       points 10
        method g-fermi
  %endblock
  %block TS.Contour.C-Right
    part circle
     from -40. eV -V/2 to -10 kT -V/2
       points 25
        method g-legendre
         opt right
  %endblock
  %block TS.Contour.T-Right
    part tail
     from prev to inf
       points 10
        method g-fermi
  %endblock
\end{fdfexample}
These contour options refer to input options for the chemical
potentials as shown in Sec.~\ref{sec:ts:chem-pot}
(p.~\pageref{sec:ts:chem-pot}). Importantly one should note the shift
of the contours corresponding to the chemical potential (the shift
corresponds to difference from the reference energy used in \tsiesta).


\subsubsection{Bias contour integration options}

The bias contour is similarly defined as the equilibrium
contours. Please use the \program{Util/TS/tselecs.sh} as a means to
create default input blocks.

\begin{fdfentry}{TS!Contours.nEq!Eta}[energy]<$\operatorname{min}[\eta_{\mathfrak e}]/10$>
  \fdfdepend{TS!Elecs!Eta}%

  The imaginary part ($\eta$) of the device states. While this may be
  set to $0$ for most systems it defaults to the minimum $\eta$ value
  for the electrodes ($\operatorname{min}[\eta_{\mathfrak
      e}]/10$). This ensures that the device broadening is always
  smaller than the electrodes while allowing broadening of localized
  states.

\end{fdfentry}

\begin{fdfentry}{TS!Contours.nEq!Fermi.Cutoff}[energy]<$5\,k_BT$>

  The bias contour is limited by the Fermi function tails. Numerically
  it does not make sense to integrate to infinity.
  %
  This energy defines where the bias integration window is turned into
  zero. Thus above $-|V|/2-E$ or below $|V|/2+E$ the DOS is defined as
  exactly zero.

\end{fdfentry}

\begin{fdfentry}{TS!Contours.nEq}[block]

  Each line defines a new contour on the non-equilibrium bias
  window. The contours defined \emph{must} be defined in
  \fdf{TS!Contour.nEq.<>}.

  These contours must all be \fdf*{part line} or \fdf*{part tail}.

\end{fdfentry}

\begin{fdfentry}{TS!Contour.nEq.<>}[block]

  This block is \emph{exactly} equivalently defined as the
  \fdf{TS!Contour.<>}. See page \pageref{TS!Contour.<>}.

\end{fdfentry}

The default options related to the non-equilibrium bias contour are
defined as this:
\begin{fdfexample}
  %block TS.Contours.nEq
    neq
  %endblock TS.Contours.nEq
  %block TS.Contour.nEq.neq
    part line
     from -|V|/2 - 5 kT to |V|/2 + 5 kT
       delta 0.01 eV
        method mid-rule
  %endblock TS.Contour.nEq.neq
\end{fdfexample}
If one chooses a different reference energy than $0$, then the limits
should change accordingly. Note that here \fdf*{kT} refers to
\fdf{TS!ElectronicTemperature}.
