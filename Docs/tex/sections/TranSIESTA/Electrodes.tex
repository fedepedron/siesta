To calculate the electronic structure of a system under external bias,
\tsiesta\ attaches the system to semi-infinite electrodes which extend
to their respective semi-infinite directions. Examples of electrodes
would include surfaces, nanowires, nanotubes or fully infinite
regions. The electrode must be large enough (in the semi-infinite
direction) so that orbitals within the unit cell only interact with a
single nearest neighbor cell in the semi-infinite direction (the size
of the unit cell can thus be derived from the range of support for the
orbital basis functions). \tsiesta\ will stop if this is not
enforced. The electrodes are generated by a separate \tsiesta\ run on
a bulk system. This implies that the proper bulk properties are
obtained by a sufficiently high $k$-point sampling. If in doubt, use
100 $k$-points along the semi-infinite direction. The results are
saved in a file with extension \sysfile{TSHS} which contains a
description of the electrode unit cell, the position of the atoms
within the unit cell, as well as the Hamiltonian and overlap matrices
that describe the electronic structure of the lead. One can generate a
variety of electrodes and the typical use of \tsiesta\ would involve
reusing the same electrode for several setups. At runtime, the
\tsiesta\ coordinates are checked against the electrode coordinates
and the program stops if there is a mismatch to a certain precision
($10^{-4}\,\mathrm{Bohr}$). Note that the atomic coordinates are
compared relatively. Hence the \emph{input} atomic coordinates of the
electrode and the device need not be the same (see e.g. the tests in
the \shell{Tests}\index{Tests} directory.

To run an electrode calculation one should do:
\begin{shellexample}
  siesta --electrode RUN.fdf
\end{shellexample}
or define these options in the electrode fdf files:
\fdf{TS!HS.Save} and \fdf{TS!DE.Save} to \fdf*{true} (the above
\code{--electrode} is a shorthand to forcefully define the two options).


\subsubsection{Matching coordinates}

Here are some rules required to successfully construct the appropriate
coordinates of the scattering region. Contrary to versions prior to
4.1, the order of atoms is largely irrelevant. One may define all
electrodes, then subsequently the device, or vice versa. Similarly,
buffer atoms are not restricted to be the first/last atoms.

However, atoms in any given electrode \emph{must} be consecutive in
the device file. I.e. if an electrode input option is given by:
\begin{fdfexample}
  %block TS.Elec.<>
    HS ../elec-<>/siesta.TSHS
    bloch 1 3 1
    used-atoms 4
    electrode-position 10
    ...
  %endblock
\end{fdfexample}
then the atoms from $10$ to $10+4*3-1$ must coincide with the atoms of
the calculation performed in the \program{../elec-<>/}
subdirectory. The above options will be discussed in the following
section.

When using the Bloch expansion (highly recommended if your system
allows it) it is advised to follow the \emph{tiling} method. However
both of the below sequences are allowed.

\paragraph{Tile} \fdfindex{TS!Elec.<>!Bloch}%
Here the atoms are copied and displaced by the full
electrode. Generally this expansion should be preferred over the
\emph{repeat} expansion due to much faster execution.
\begin{fdfexample}
  iaD = 10 ! as per the above input option
  do iC = 0 , nC - 1
  do iB = 0 , nB - 1
  do iA = 0 , nA - 1
    do iaE = 1 , na_u
      xyz_device(:, iaD) = xyz_elec(:, iaE) + &
          cell_elec(:, 1) * iA + &
          cell_elec(:, 2) * iB + &
          cell_elec(:, 3) * iC
      iaD = iaD + 1
    end do
  end do
  end do
  end do
\end{fdfexample}

By using \sisl\cite{sisl} one can achieve the tiling scheme
by using the following command-line utility on an input
\program{ELEC.fdf} structure with the minimal electrode:
\begin{codeexample}
  sgeom -tx 1 -ty 3 -tz 1 ELEC.fdf DEVICE_ELEC.fdf
\end{codeexample}

\paragraph{Repeat} \fdfindex{TS!Elec.<>!Bloch}%
Here the atoms are copied individually. Generally
this expansion should \emph{not} be used since it is much slower than
tiling.
\begin{fdfexample}
  iaD = 10 ! as per the above input option
  do iaE = 1 , na_u
    do iC = 0 , nC - 1
    do iB = 0 , nB - 1
    do iA = 0 , nA - 1
      xyz_device(:, iaD) = xyz_elec(:, iaE) + &
          cell_elec(:, 1) * iA + &
          cell_elec(:, 2) * iB + &
          cell_elec(:, 3) * iC
      iaD = iaD + 1
    end do
    end do
    end do
  end do
\end{fdfexample}

By using \sisl\cite{sisl} one can achieve the repeating scheme by
using the following command-line utility on an input
\program{ELEC.fdf} structure with the minimal electrode:
\begin{codeexample}
  sgeom -rz 1 -ry 3 -rx 1 ELEC.fdf DEVICE_ELEC.fdf
\end{codeexample}



\subsubsection{Principal layer interactions} %
\index{transiesta!electrode!principal layer}%

It is \emph{extremely} important that the electrodes only interact
with one neighboring supercell due to the self-energy
calculation\cite{Sancho1985}. \tsiesta\ will print out a block as this
(\shell{<>} is the electrode name):
\begin{verbatim}
 <> principal cell is perfect!
\end{verbatim}
if the electrode is correctly setup and it only interacts with its
neighboring supercell.
%
In case the electrode is erroneously setup, something similar to the
following will be shown in the output file.
\begin{verbatim}
 <> principal cell is extending out with 96 elements:
    Atom 1 connects with atom 3
    Orbital 8 connects with orbital 26
    Hamiltonian value: |H(8,6587)|@R=-2 =  0.651E-13 eV
    Overlap          :  S(8,6587)|@R=-2 =   0.00
\end{verbatim}
It is imperative that you have a \emph{perfect} electrode as otherwise
nonphysical results will occur. This means that you need to add more
layers in your electrode calculation (and hence also in your
scattering region). An example is an ABC stacking electrode. If the
above error is shown one \emph{has} to create an electrode with ABCABC
stacking in order to retain periodicity.

By default \tsiesta\ will die if there are connections beyond the
principal cell. One may control whether this is allowed or not by
using \fdf{TS!Elecs!Neglect.Principal}.

