
siesta_add_library(${PROJECT_NAME}.mpi
  NO_LINKER_FLAGS
  OBJECT

    # Sources
    mpi_siesta.F90
    mpi_interfaces.F90
    mpi__include.f90
    Interfaces_integer.f90
    Interfaces.f90
)

# We need to find "Interfaces.f90", which is generated in the build
#("binary") directory, so we need to add this directory to the list
# The following line will work also, but it is better practice to have
# target-specific settings
###set(CMAKE_INCLUDE_CURRENT_DIR ON)
target_include_directories(${PROJECT_NAME}.mpi PRIVATE "${CMAKE_CURRENT_BINARY_DIR}")

target_compile_definitions(
  ${PROJECT_NAME}.mpi
  PUBLIC
  "$<$<BOOL:${SIESTA_WITH_GRID_SP}>:GRID_SP>"
  "$<$<BOOL:${SIESTA_WITH_NO_MPI_INTERFACES}>:NO_MPI_INTERFACES>"
)

target_link_libraries(${PROJECT_NAME}.mpi PUBLIC MPI::MPI_Fortran)
# So that clients of ${PROJECT_NAME}.mpi can find its modules:
target_include_directories(${PROJECT_NAME}.mpi INTERFACE ${CMAKE_CURRENT_BINARY_DIR})


# Handling of interface-generation script
# These generation scripts do not work with the suffix for the executables
siesta_suffix(
  NEW ""
  VARIABLES EXECUTABLE)


# Create integer kinds and interfaces
siesta_add_executable(${PROJECT_NAME}.mpi_int_explorer
  # Do not default to build this one, it is only
  # a temporary build dependency
  EXCLUDE_FROM_ALL
  int_explorer.f90
  )


list(JOIN SIESTA_INTEGER_KINDS " " SIESTA_INTEGER_KINDS_STR)
add_custom_command(
  OUTPUT Interfaces_integer.f90
  # Dependency targets
  DEPENDS ${PROJECT_NAME}.mpi_int_explorer
  COMMAND sh "${CMAKE_CURRENT_SOURCE_DIR}/generate_integer.sh" ${SIESTA_INTEGER_KINDS_STR}
  VERBATIM
  COMMENT "Creating MPI interfaces and modules for integer kinds"
  )


# Create real kinds and interfaces
siesta_add_executable(${PROJECT_NAME}.mpi_kind_explorer
  EXCLUDE_FROM_ALL
  kind_explorer.f90
  )

list(JOIN SIESTA_REAL_KINDS " " SIESTA_REAL_KINDS_STR)
add_custom_command(
  OUTPUT Interfaces.f90
  DEPENDS ${PROJECT_NAME}.mpi_kind_explorer
  COMMAND sh "${CMAKE_CURRENT_SOURCE_DIR}/generate_real.sh" ${SIESTA_REAL_KINDS_STR}
  VERBATIM
  COMMENT "Creating MPI interfaces and modules for real, character and logical"
  )

# Restore the old suffix before using this one.
siesta_suffix(
  POP
  VARIABLES EXECUTABLE
  )
